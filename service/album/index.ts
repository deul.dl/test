import Albums, { Album } from '../../models/albums'
import Photos from '../../models/photos';

interface AlbumInput {
  title: string;
  owner: string;
  id?: number
}

interface Errors {
  title?: String
}

export interface AlbumOut {
  errors?: Errors
  album?: Album
}

export const create = async (data: AlbumInput): Promise<AlbumOut> => {
  let errors: Errors = {};
	const title = typeof data.title == 'string' ? data.title.trim().toLowerCase() : '';
	if (!title) {
		errors.title = 'Empty field';
    return { errors };
	}

  const album = await Albums.create(data);
  return { album: album.toObject() };
}

export const deleteById = async (id: string, owner: string): Promise<number> => {
  await Photos.deleteMany({ albumId: { $eq: id }});
  return (await Albums.deleteOne({ id: { $eq: id }, owner: { $eq: owner } })).deletedCount;
}

export const update = async (data: AlbumInput): Promise<AlbumOut> => {
  let errors: Errors = {};
	const title: string = typeof data.title == 'string' ? data.title.trim().toLowerCase() : '';
	if (!title) {
		errors.title = 'Empty field';
    return { errors };
	}

  const album = (await Albums.findOneAndUpdate(
    { id: data.id, owner: data.owner },
    { $set: { title: data.title }
  })).toObject();
  return { album }
}