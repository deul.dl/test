import axios from 'axios'

import Albums, { Album } from '../../models/albums';
import Photos, { Photo } from '../../models/photos';

interface RequestPhoto {
  albumId: number
  id?: number
  title: string
  url: string
  thumbnailUrl: string
}

export const loadPhotos = async (owner: string): Promise<void> => {
    const photos: RequestPhoto[] = (await axios.get('http://jsonplaceholder.typicode.com/photos')).data;
    
    const albums: Album[] = photos.reduce((acc: Album[], curr: RequestPhoto) => {
      const isFoundAlbum: boolean = acc.some((album: Album) => album.id === curr.albumId);
      if (!isFoundAlbum) {
        return acc.concat([{ owner, id: curr.albumId, title: curr.albumId.toString() }]);
      }
      return acc;
    }, <Album[]>[]);
    await Albums.create(albums);
    await Photos.create(photos.map<RequestPhoto>(({ id, ...photo }) => photo));
}

export const deletePhotosByIds = async (ids: string[]): Promise<number> => {
  return (await Photos.deleteMany({ _id: { $in: ids } })).deletedCount;
}

interface GetPhotosInput {
  owner?: string
  max: number
  page: number
}

interface GetPhotosOutput {
  photos: Photo[]
  max: number
  page: number
}



export const getPhotos = async (input: GetPhotosInput): Promise<GetPhotosOutput> => {
  const $skip: number = (input.page * input.max) - input.max;
  const photos: Photo[] = await Photos.aggregate([
    {
      $lookup: {
        from: 'albums',
        localField: 'albumId',
        foreignField: 'id',
        as: 'albums',
      },
    },
    {
      $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$albums", 0 ] }, "$$ROOT" ] } }
    },
    { $limit: $skip + input.max },
    { $skip },
    { $project: { albums: 0 }},
    ...(input.owner ? [{ $match: { owner: input.owner } }] : []),
  ]);
  const photosOut: GetPhotosOutput = {
    photos,
    max: input.max,
    page: input.page
  }
  return photosOut
}