import * as bcrypt from 'bcrypt'

import Users, { User } from '../models/users'
import Validator from '../utils/validator'

const PASSWORD_LENGTH_MIN = 8;
const PASSWORD_LENGTH_MAX = 16;
const LOGIN_LENGTH_MAX = 50;

interface LoginInput {
  username: string
  password: string
}

interface SignupInput {
  username: string
  password: string
  email: string
}

interface Errors {
  username?: string
  password?: string
  email?: string
}

export interface AuthOut {
  errors?: Errors
  user?: User
}

export const login = async (data: LoginInput): Promise<AuthOut> => {
  let errors: Errors = {};
	const username: string = typeof data.username == 'string' ? data.username.trim().toLowerCase() : '';
	if (!username) {
		errors.username = 'Empty field';
    return { errors };
	} else if (username.length > LOGIN_LENGTH_MAX) {
		errors.username = 'Too long';
    return { errors };
	}

	const password: string = typeof data.password == 'string' ? data.password : '';
	if (!password.trim()) {
		errors.password = 'Empty field';
    return { errors };
	} else if (password.length < PASSWORD_LENGTH_MIN) {
		errors.password = 'Too short';
    return { errors };
	} else if (password.length > PASSWORD_LENGTH_MAX) {
		errors.password = 'Too long';
    return { errors };
	}

  const user = await Users.findOne({ $or: [{ email: username }, { username }] })
  
  const result = await bcrypt.compare(password, user.password_hash);

  if (result) {
    return { user: user.toObject() }; 
  }

  throw new Error('Incorrect password');
}

export const signup = async (data: SignupInput): Promise<AuthOut> => {
  let errors: Errors = {};
	const username: string = typeof data.username == 'string' ? data.username.trim().toLowerCase() : '';
	if (!username) {
		errors.username = 'Empty field';
    return { errors };
	} else if (username.length > LOGIN_LENGTH_MAX) {
		errors.username = 'Too long';
    return { errors };
	}

  const email: string = typeof data.email == 'string' ? data.email : '';
  if (!email.trim()) {
    errors.email = 'Empty email'
    return { errors };
  } else if (!Validator.email(email)) {
		errors.email = 'Email is incorrect';
    return { errors };
	}

  const password: string = typeof data.password == 'string' ? data.password : '';
	if (!password.trim()) {
		errors.password = 'Empty field';
    return { errors };
	} else if (password.length < PASSWORD_LENGTH_MIN) {
		errors.password = 'Too short';
    return { errors };
	} else if (password.length > PASSWORD_LENGTH_MAX) {
		errors.password = 'Too long';
    return { errors };
	}

  const passwordHash: string = await bcrypt.hash(password, 10);

  await Users.create({ username, email, password_hash: passwordHash })
}