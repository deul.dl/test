import { Schema, Types, model } from 'mongoose'

export interface Album {
  id?: number
  _id?: Types.ObjectId
  title: string
  owner: string
  created_at?: Date
}

const schema = new Schema<Album>({
  id: { type: Number, required: true },
  title: { type: String, required: true },
  owner: { type: String, required: true },
}, {
  timestamps: { createdAt: 'created_at' }
})

export default model<Album>('albums', schema)
