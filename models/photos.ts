import { Schema, model } from 'mongoose'

export interface Photo {
  albumId: number
  title: String
  url: String
  thumbnailUrl: string
}

const schema = new Schema<Photo>({
  albumId: { type: Number, required: true },
  title: { type: String, required: true },
  url: { type: String, required: true },
  thumbnailUrl: { type: String, required: true },
}, {
  timestamps: { createdAt: 'created_at' }
});

export default model<Photo>('photos', schema)
