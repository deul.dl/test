import { Schema, Types, model } from 'mongoose'

export interface User {
  username: string;
  email: string;
  password_hash: string;
  created_at?: Date
  _id?: Types.ObjectId;
}

const schema = new Schema<User>({
  username: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password_hash: { type: String, required: true },
}, {
  timestamps: { createdAt: 'created_at' }
});

export default model<User>('users', schema)
