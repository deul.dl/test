import * as express from 'express';

import * as album from '../../service/album';
import Response from '../../utils/ApiResponse';

const router = express.Router();

router.use('/photos', require('./photo'));

router
  .put('/change/:id', async (req: express.Request, res: express.Response) => {
    try {
      const albumOutData: album.AlbumOut = await album.update({
        owner: req.session.user._id.toString(),
        id: parseInt(req.params.id, 10),
        ...req.body
      });
      if (albumOutData?.errors) return res.json(new Response().data(albumOutData));
      return res.status(200).json(new Response().data(albumOutData));
    } catch (err) {
      return res.status(500).json(new Response().error(err.message));
    }
  })
  .delete('/delete/:id', async (req: express.Request, res: express.Response) => {
    try {
      const count: number = await album.deleteById(req.params.id, req.session.user._id.toString());
      return res.status(200).json(new Response().data({ count }));
    } catch (err) {
      return res.status(500).json(new Response().error(err.message));
    }
  });

  module.exports = router;