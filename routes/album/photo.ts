import * as express from 'express';

import * as photo from '../../service/album/photo';
import Response from '../../utils/ApiResponse';

const router = express.Router();

router
  .get('/load', async (req: express.Request, res: express.Response) => {
    try {
      await photo.loadPhotos(req.session.user._id.toString());
      return res.status(200).json(new Response().ok(true));
    } catch (err) {
      return res.status(500).json(new Response().error(err.message));
    }
  })
  .delete('/delete', async (req: express.Request, res: express.Response) => {
    try {
      const photoIds: string[] = (req.query.ids as string).split(',');
      const count = await photo.deletePhotosByIds(photoIds);
      return res.status(200).json(new Response().data({ count }));
    } catch (err) {
      return res.status(500).json(new Response().error(err.message));
    }
  });
  
  module.exports = router;