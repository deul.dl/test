import * as express from 'express';

import * as auth from '../service/auth';
import Response from '../utils/ApiResponse';

const router = express.Router();

router
  .post('/register', async (req: express.Request, res: express.Response) => {
    try {
      const signOutData: auth.AuthOut = await auth.signup(req.body);
      if (signOutData?.errors) return res.json(new Response().data(signOutData));
      return res.json(new Response().ok(true));
    } catch (err) {
      return res.status(500).json(new Response().error(err.message));
    }
  })
  .post('/login', async (req: express.Request, res: express.Response) => {
    try {
      const signInData: auth.AuthOut = await auth.login(req.body);
      if (signInData?.errors) return res.json(new Response().data(signInData));
      req.session.user = signInData.user;
      return res.json(new Response().ok(true));
    } catch (err) {
      return res.status(500).json(new Response().error(err.message));
    }
  })
  .post('/logout', (req: express.Request, res: express.Response) => {
    req.session.destroy((err?: Error) => {
      if (err) return res.status(400).json(new Response().error(err.message));
      return res.status(200).json(new Response().ok(true));
    });
  });

module.exports = router