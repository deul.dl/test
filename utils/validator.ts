class Validator {
	static email (str: string = '') {
		// eslint-disable-next-line no-useless-escape
		return !!str.match(/^\w+(([\.\-\_\+\w]+)\w+)*@([\w\-\_\+]+)+([\.\-\_\+]?\w+)*(\.\w{2,10})+$/);
	}
}

export default Validator;
