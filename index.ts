import * as express from 'express'
import * as http from 'http'
import * as bodyParser from 'body-parser'
import * as session from 'express-session'
import * as mongoose from 'mongoose'

import * as photo from './service/album/photo';
import { User } from './models/users'
import ApiResponse from './utils/ApiResponse'

const app = express();

mongoose.connect('mongodb://localhost/test')
	.then(() => console.log('Database connected!'))
	.catch(console.error);

app.use((_, res: express.Response, next: express.NextFunction) => {
	res.setHeader('Content-Type', 'application/json');
	next();
});

app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(session({
	name: 'SID',
	secret: 'Warcraft 3 is the best game!!!',
	resave: false,
	saveUninitialized: true,
	proxy: false,
	cookie: {
		sameSite: 'none',
		secure: false,
		maxAge: 3600000 * 24 * 30,
		path: '/',
	}
}));

declare module 'express-session' {
  interface SessionData {
    user: User;
  }
}

const isUser = (req: express.Request, res: express.Response, next: express.NextFunction) => {
	if (req.session.user) {
		return next();
	}
	return res.status(403).json(new ApiResponse().error('Not allowed!'));
}

app
	.use('/auth', require('./routes/auth'))
	.use('/albums', isUser, require('./routes/album'))
	.get('/get-photos', async (req: express.Request, res: express.Response) => {
		try {
			const owner: string = req.query.owner as string;
			const data = await photo.getPhotos({
				owner,
				page: parseInt(req.query.page as string) || 1,
				max: parseInt(req.query.max as string) || 10
			});
			return res.status(200).json(new ApiResponse().data(data));
		} catch (err) {
			return res.status(500).json(new ApiResponse().error(err.message));
		}
	});

const server = http.createServer(app)

server.listen(3000, (error?: Error) => {
	if (error) {
		console.error(error.message);
	} else {
		const addr = server.address();
		const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
		console.log(`Listening on ${bind}`);
	}
});